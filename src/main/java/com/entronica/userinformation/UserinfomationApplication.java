package com.entronica.userinformation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserinfomationApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserinfomationApplication.class, args);
	}

}
