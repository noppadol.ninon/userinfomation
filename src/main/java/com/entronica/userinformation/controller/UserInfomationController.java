package com.entronica.userinformation.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.entronica.userinformation.entity.ContactInformation;
import com.entronica.userinformation.entity.EducationalInformation;
import com.entronica.userinformation.entity.ExperienceInformation;
import com.entronica.userinformation.entity.GuildInformation;
import com.entronica.userinformation.entity.InterestInformation;
import com.entronica.userinformation.entity.SkillInformation;
import com.entronica.userinformation.entity.UserInformation;
import com.entronica.userinformation.request.UserInformationRequest;
import com.entronica.userinformation.service.UserInformationService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class UserInfomationController {
	
	@Autowired
	UserInformationService userInformationService;
	
	@PostMapping(value="/addUserInformation", consumes = {MediaType.APPLICATION_JSON_VALUE,MediaType.MULTIPART_FORM_DATA_VALUE } )
	public ResponseEntity<UserInformationRequest> addUserInformation(@RequestPart("userInforamtionRequest") String userInforamtionRequest,
																	 @RequestPart("imageUser")  MultipartFile   imgUser){
		
		UserInformationRequest userInformationObj = null;
		
		try {
			ObjectMapper objMapp = new ObjectMapper();
			userInformationObj = new UserInformationRequest();
			userInformationObj = objMapp.readValue(userInforamtionRequest, UserInformationRequest.class);
			
			String fileName = imgUser.getResource().getFilename();
			//System.out.println("---> "+fileName);
			
			String base64StringFile = Base64.getEncoder().encodeToString(imgUser.getBytes());

	        System.out.println(base64StringFile);
	        userInformationObj.setBase64File(base64StringFile);
	        userInformationObj.setImgName(fileName);
		
			UserInformation userInformation = new UserInformation();
			
			userInformation.setUsername(userInformationObj.getUsername());
			userInformation.setFirstName(userInformationObj.getFirstName());
			userInformation.setLastName(userInformationObj.getLastName());
			userInformation.setNickName(userInformationObj.getNickName());
			userInformation.setPosition(userInformationObj.getPosition());
			userInformation.setNationality(userInformationObj.getNationality());
			userInformation.setTelephoneNumber(userInformationObj.getTelephoneNumber());
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
			userInformation.setStartingDate(formatter.parse(userInformationObj.getStartingDate()));
			userInformation.setImgName(userInformationObj.getImgName());
			userInformation.setBase64File(userInformationObj.getBase64File());
			
			//Contact Information Entity
			ContactInformation contactInformation = new ContactInformation();
			contactInformation.setAddress(userInformationObj.getContactInformation().getAddress());
			contactInformation.setSubDistrict(userInformationObj.getContactInformation().getSubDistrict());
			contactInformation.setDistrict(userInformationObj.getContactInformation().getDistrict());
			contactInformation.setProvince(userInformationObj.getContactInformation().getProvince());
			contactInformation.setPostalCode(userInformationObj.getContactInformation().getPostalCode());
			contactInformation.setFacebook(userInformationObj.getContactInformation().getFacebook());
			contactInformation.setLineId(userInformationObj.getContactInformation().getLineId());
			contactInformation.setInstragram(userInformationObj.getContactInformation().getInstragram());
			userInformation.setContactinformation(contactInformation);
			
			//Education Entity
			Set<EducationalInformation> eduList = new HashSet<>(); 
			userInformationObj.getEducationInformation().forEach(eu -> {
				EducationalInformation educationInformation = new EducationalInformation();
				educationInformation.setYear(eu.getYear());
				educationInformation.setUniversityName(eu.getUniversityName());
				eduList.add(educationInformation);
			});
			userInformation.setEducationalInformation(eduList);
			
			//Experience Entity
			Set<ExperienceInformation> expList = new HashSet<>();
			userInformationObj.getExperienceInformation().forEach(ex->{
				ExperienceInformation experienceInformation = new ExperienceInformation();
				experienceInformation.setExperience(ex.getExperience());
				expList.add(experienceInformation);
			});
			userInformation.setExperienceInformation(expList);
			
			//SkillInformation Entity
			Set<SkillInformation> skList = new HashSet<>();
			userInformationObj.getSkillInformation().forEach(sk->{
				SkillInformation skillInfo = new SkillInformation();
				skillInfo.setSkill(sk.getSkill());
				skillInfo.setLevel(sk.getLevel());
				skList.add(skillInfo);
			});
			userInformation.setSkillInformation(skList);
			
			//Interest Entity
			Set<InterestInformation> inList = new HashSet<>();
			userInformationObj.getInterestInformation().forEach(in -> {
				InterestInformation interestInformation = new InterestInformation();
				interestInformation.setInterest(in.getInterest());
				inList.add(interestInformation);
			});
			userInformation.setInterestInformation(inList);
			
			//GuildInformation Entity
			Set<GuildInformation> guList = new HashSet<>();
			userInformationObj.getGuildInformation().forEach(gu -> {
				GuildInformation guildInformation = new GuildInformation();
				guildInformation.setGuild(gu.getGuild());
				guList.add(guildInformation);
			});
			userInformation.setGuildInformation(guList);
			
			userInformationService.saveInformation(userInformation);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return new ResponseEntity<UserInformationRequest>(userInformationObj,HttpStatus.OK);
	} 
	
	@GetMapping(value="/findAllUserInformation")
	public ResponseEntity<List<UserInformation>> findUserInformation(){
		
		List<UserInformation> userInformation = userInformationService.findUserInformationList();
		return new ResponseEntity<List<UserInformation>>(userInformation,HttpStatus.OK);
	}
	
}
