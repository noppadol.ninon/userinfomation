package com.entronica.userinformation.entity;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserInformation {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String username;
	private String firstName;
	private String lastName;
	private String nickName;
	private String position;
	private String nationality;
	private String telephoneNumber;
	@Lob
	private String base64File;
	private String imgName;
	private Date startingDate;
	
	@OneToOne(cascade=CascadeType.ALL)
	//@JoinColumn(name="userInformation_id")
	private ContactInformation contactinformation;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="userInformation_id")
	private Set<EducationalInformation> educationalInformation;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="userInformation_id")
	private Set<ExperienceInformation> ExperienceInformation;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="userInformation_id")
	private Set<SkillInformation> SkillInformation;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="userInformation_id")
	private Set<InterestInformation> interestInformation;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="userInformation_id")
	private Set<GuildInformation> GuildInformation;
	
}
