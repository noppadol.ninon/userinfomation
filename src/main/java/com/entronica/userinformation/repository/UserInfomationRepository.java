package com.entronica.userinformation.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.entronica.userinformation.entity.UserInformation;

//@Repository
public interface UserInfomationRepository extends JpaRepository<UserInformation, Long> {

	@Query("SELECT u FROM UserInformation u")
	List<UserInformation> findUserInformationList();
	
}
