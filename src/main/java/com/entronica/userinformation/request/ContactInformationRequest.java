package com.entronica.userinformation.request;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContactInformationRequest {
	
	private String address;
	private String subDistrict;
	private String district;
	private String province;
	private String postalCode;
	private String facebook;
	private String lineId;
	private String instragram;

}
