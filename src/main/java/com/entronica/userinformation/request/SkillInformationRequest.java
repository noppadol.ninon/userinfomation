package com.entronica.userinformation.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SkillInformationRequest {
	
	private String skill;
	private String level;

}
