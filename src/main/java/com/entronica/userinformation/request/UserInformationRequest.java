package com.entronica.userinformation.request;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInformationRequest {
	
	private String username;
	private String firstName;
	private String lastName;
	private String nickName;
	private String position;
	private String nationality;
	private String telephoneNumber;
	private String startingDate;
	private String imgName;
	private String base64File;
	
	private ContactInformationRequest contactInformation;
	
	private List<EducationInformationRequest> educationInformation;
	
	private List<ExperienceInformationRequest> experienceInformation;
	
	private List<SkillInformationRequest> skillInformation;
	
	private List<InterestInformationRequest> interestInformation;
	
	private List<GuildInformationRequest> guildInformation;
}
