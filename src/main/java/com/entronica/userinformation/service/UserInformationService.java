package com.entronica.userinformation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entronica.userinformation.entity.UserInformation;
import com.entronica.userinformation.repository.UserInfomationRepository;

@Service
public class UserInformationService {
	
	@Autowired
	UserInfomationRepository userInfomationRepository;
	
	public void saveInformation(UserInformation userInformation) {
		userInfomationRepository.save(userInformation);
	}
	
	public List<UserInformation> findUserInformationList(){
		return userInfomationRepository.findUserInformationList();
	}

}
